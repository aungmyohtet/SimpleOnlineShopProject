package com.eieimon.project.demo.service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eieimon.project.demo.entity.ProductImage;

@Service
public class ProductImageServiceImpl implements ProductImageService {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    @Transactional
    public void saveImagePath(ProductImage productImage) {
        // TODO Auto-generated method stub
        if (productImage.getId() == null) {
            entityManager.persist(productImage);
        } else {
            entityManager.merge(productImage);
        }

    }

    @Override
    @Transactional
    public ProductImage findById(Long id) {
        return entityManager.find(ProductImage.class, id);
    }

    @Override
    public ProductImage findByProductId(long pid) {
        Query query = entityManager.createQuery("SELECT p FROM ProductImage p WHERE p.product.id =:id");
        query.setParameter("id", pid);
        return (ProductImage) query.getResultList().get(0);
    }

}
