package com.eieimon.project.demo.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eieimon.project.demo.entity.Category;
import com.eieimon.project.demo.entity.Product;

@Service
public class ProductServiceImpl implements ProductService {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    @Transactional
    public void save(Product product) {
        // TODO Auto-generated method stub
        if (product.getId() == null) {
            entityManager.persist(product);
        } else {
            entityManager.merge(product);
        }

    }

    @Override
    @Transactional
    public List<Product> getProductList() {
        Query query = entityManager.createQuery("SELECT p FROM Product p");
        List results = query.getResultList();
        return results;
    }

    @Override
    @Transactional
    public Product getProducts(Long id) {
        Query query = entityManager.createQuery("SELECT p FROM Product p WHERE p.id =:id");
        query.setParameter("id", id);
        Product product = (Product) query.getSingleResult();
        System.out.println(">>>>>>>>>>>>>>" + product.getId());
        System.out.println(">>>>>>>>>>>>>>" + product.getName());
        return product;
    }

    @Override
    @Transactional
    public List<Product> getProductsbyCategory(Long id) {
        Query query = entityManager.createQuery("SELECT p FROM Product p WHERE p.id=:id");
        query.setParameter("id", id);
        List<Product> productList = query.getResultList();
        return productList;
    }

}
