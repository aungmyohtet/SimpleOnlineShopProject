package com.eieimon.project.demo.service;

import com.eieimon.project.demo.entity.User;

public interface UserService {
    public void save(User user);

    public User findById(Long id);

    public User findByEmail(String email);
}